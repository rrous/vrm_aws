import boto3
from Auth import DB_NAME, PROXY_NAME, TBL_NAME, VERIFY_SSL, VRM_LOGIN, VRM_PASS, INST_ID
from dateutil import parser
from datetime import date
import json

client = boto3.client('timestream-query', verify=VERIFY_SSL)
#query_string = f"SELECT time FROM {DB_NAME}.{TBL_NAME} ORDER BY time DESC LIMIT 1"
query_string = f"SELECT * FROM {DB_NAME}.{TBL_NAME} ORDER BY time"
try:
    result = client.query(QueryString=query_string)
except Exception as err:
    print("Exception while running query:", err)
#readyData = parser.parse(result['Rows'][0]['Data'][0]['ScalarValue'])

print(result)

with open(str(date.today()) + '_dump.json', 'w') as file:
    print(json.dumps(result, indent=4, sort_keys=False), file=file)