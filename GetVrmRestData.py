import json
import string
from typing import List

from datetime import *
from dateutil import parser
from vrm import VRM_API
from Auth import DB_NAME, PROXY_NAME, TBL_NAME, VERIFY_SSL, VRM_LOGIN, VRM_PASS, INST_ID
from utils import isfloat
from itertools import groupby
from operator import itemgetter
import boto3

api = VRM_API(
    username=VRM_LOGIN,
    password=VRM_PASS,
    proxies=PROXY_NAME,
    verify=VERIFY_SSL
)

def parseVRMstats(a : json, reading_type : string, path : List[str], measure_name : string):
    inputData = []
    for i in path: a = a[i]
    for item in a: #enumarate(a)
        if not isfloat(item[1]): continue  # must be convertible to float
        
        if item[0] < 100000000000: item[0] = int(item[0] * 1000) # from seconds to miliseconds
        item[0] = 3600000 * int(item[0]/3600000) #only whole hours interval
        time = datetime.fromtimestamp(item[0] / 1000).strftime('%Y-%m-%d %H:%M:%S')
        
        if reading_type == "dc": item[1] = item[1] / 1000  #DC Watts to kW
        amount = float(round(item[1], 4))
        
        inputData.append({
            'Measurement_Code': reading_type,
            'measure_name': measure_name,
            'Time': time,
            'Value': amount,
            'Timestamp': item[0]
        })
    
    grouper = itemgetter('Measurement_Code', 'Time', 'measure_name')
    out = []
    for key, grp in groupby(sorted(inputData, key = grouper), grouper): #grouping(AVG) values from the same hour
        temp_dict = dict(zip(['Measurement_Code', 'Time', 'measure_name'], key))
        objects = list(grp)
        temp_list = [item["Value"] for item in objects]
        temp_dict["Value"] = float(round(sum(temp_list) / len(temp_list),4))
        temp_list = [item["Timestamp"] for item in objects]
        temp_dict["Timestamp"] = min(temp_list) 
        out.append(temp_dict)

    # from pprint import pprint
    # pprint(out)
    
    return out

def VRM_getStats(start, end, type='kwh'):
    rawdata = api.get_consumption_stats(INST_ID, start=start, end=end, type=type)
    return rawdata

def VRM_getBatteryCharge(start, end):
    rawdata = api.get_custom_stats(INST_ID, ['bs'], start, end)
    return rawdata

def VRM_getDC(start, end):
    rawdata = api.graph_widgets(INST_ID, ['dc'], None, start, end)
    return rawdata

def prepareData(start, end):
    vrmStats = VRM_getStats(start, end)
    try:
        readyData = parseVRMstats(vrmStats, 'Bc', ['records', 'Bc'], 'energy_flow')
        readyData.extend(parseVRMstats(vrmStats, 'Pc', ['records', 'Pc'],'energy_flow'))
        readyData.extend(parseVRMstats(vrmStats, 'Pb', ['records', 'Pb'],'energy_flow'))
    except Exception as err:
        print("Exception while parsing VRM output - Photovoltaic:", err)
        return(False)

    vrmStats = VRM_getBatteryCharge(start, end)
    try:
        readyData.extend(parseVRMstats(vrmStats, 'bs', ['records', 'bs'], 'battery_charge'))
    except Exception as err:
        print("Exception while parsing VRM output - Battery charge:", err)
        return(False)

    vrmStats = VRM_getDC(start, end)
    try:
        readyData.extend(parseVRMstats(vrmStats, 'dc', ['records', 'data', '140'],'energy_flow'))
    except Exception as err:
        print("Exception while parsing VRM output - DC power:", err)
        return(False)
    
    return readyData

def AWS_GetLastRecordTime(): ### TO DEL from there
    client = boto3.client('timestream-query', verify=VERIFY_SSL)
    query_string = f"SELECT time FROM {DB_NAME}.{TBL_NAME} ORDER BY time DESC LIMIT 1"
    try:
        result = client.query(QueryString=query_string)
    except Exception as err:
        print("Exception while running query:", err)
    return parser.parse(result['Rows'][0]['Data'][0]['ScalarValue'])

# start = AWS_GetLastRecordTime() + timedelta(hours=1)
# #start = datetime.now() - timedelta(hours=72)
# start = start.replace(minute=0, second=0, microsecond=0)
# end = datetime.now() - timedelta(hours=3)
# end = end.replace(minute=59, second=58, microsecond=0)
# print(start,'  ', end)
# print(prepareData(start,end))

# with open('2022-07-07.json', 'r') as file:
#     print(json.load(file))

# app.run(host = '0.0.0.0', port = 8080)