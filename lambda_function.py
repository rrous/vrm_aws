from GetVrmRestData import prepareData

from datetime import *
from dateutil import parser
from Auth import DB_NAME, PROXY_NAME, TBL_NAME, VERIFY_SSL

import boto3

def post_to_AWS(inData):
    #myConfig = Config(proxies=PROXY_NAME)
    client = boto3.client('timestream-write', verify=VERIFY_SSL) #config=myConfig
    version = int(datetime.now().timestamp())

    #logfile = open('log.txt', 'w')

    for record in inData:
        
        dimensions = [
            {'Name': 'Site', 'Value': 'OstrovniDum1'}, 
            {'Name': 'Measurement_Code', 'Value': record["Measurement_Code"]}
        ]

        common_attributes = {
                'Dimensions': dimensions,
                'MeasureValueType': 'DOUBLE',
                'Time': str(record["Timestamp"]),
                'Version': version # UPSERT -> later version is showed
            }

        aws_record = [
            {'MeasureName': record["measure_name"], 'MeasureValue': str(record["Value"])}
        ]

        try:
            print(common_attributes," - ", aws_record)
            result = client.write_records(DatabaseName=DB_NAME, TableName=TBL_NAME, Records=aws_record, CommonAttributes = common_attributes)
            print("WriteRecords Status: [%s]" % result['ResponseMetadata']['HTTPStatusCode'])
        except client.exceptions.RejectedRecordsException as err:
            print("RejectedRecords: ", err)
            for rr in err.response["RejectedRecords"]:
                print("Rejected Index " + str(rr["RecordIndex"]) + ": " + rr["Reason"])
            print("Other records were written successfully. ")
        except Exception as err:
            print("Error:", err)
    
    #logfile.close()

def AWS_GetLastRecordTime():
    client = boto3.client('timestream-query', verify=VERIFY_SSL)
    query_string = f"SELECT time FROM {DB_NAME}.{TBL_NAME} ORDER BY time DESC LIMIT 1"
    try:
        result = client.query(QueryString=query_string)
    except Exception as err:
        print("Exception while running query:", err)
    return parser.parse(result['Rows'][0]['Data'][0]['ScalarValue'])

def lambda_handler(event, context):
    start = AWS_GetLastRecordTime() + timedelta(hours=1) #start = datetime.now() - timedelta(hours=72)
    start = start.replace(minute=0, second=0, microsecond=0)
    end = datetime.now() - timedelta(hours=3)
    end = end.replace(minute=59, second=58, microsecond=0)
    print(start,'  ', end)
    post_to_AWS(prepareData(start,end))

lambda_handler(True, True)

# with open('2022-07-07.json', 'r') as file:
#     toAWS(json.load(file))
