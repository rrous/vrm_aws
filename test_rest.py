from asyncio.windows_events import NULL
from ssl import VerifyMode
import requests
from requests_kerberos import HTTPKerberosAuth
from urllib3.util import parse_url
from Auth import PROXY_NAME, VERIFY_SSL, VRM_LOGIN, VRM_PASS
#from requests.auth import HTTPProxyAuth
#import requests_negotiate
import os
import json
#import sys

class HTTPAdapterWithProxyKerberosAuth(requests.adapters.HTTPAdapter):
    def proxy_headers(self, proxy):
        headers = {}
        auth = HTTPKerberosAuth()
        negotiate_details = auth.generate_request_header(None, parse_url(proxy).host, is_preemptive=True)
        headers['Proxy-Authorization'] = negotiate_details
        return headers

os.system('cls')
headers = NULL

session = requests.Session()
# if PROXY_NAME:
#     session.proxies = PROXY_NAME
#     session.mount('https://', HTTPAdapterWithProxyKerberosAuth())

api_url = "https://vrmapi.victronenergy.com/v2/auth/login"
api_data = f"""{{
    "username": "{VRM_LOGIN}",
    "password": "{VRM_PASS}"
}}"""

response = session.get(r"https://www.google.com/", proxies=PROXY_NAME, verify=VERIFY_SSL)
print(response)

headers = session.post(api_url, data=api_data, verify=VERIFY_SSL)
token = "Bearer " + headers.json()["token"]
idUser = headers.json()["idUser"]
print(token, " ", idUser)

headers = {"X-Authorization" : token}

file = open('example.json', 'w')
api_url = "https://vrmapi.victronenergy.com/v2/users/{}/installations?extended=1".format(idUser)
# api_url = f"https://vrmapi.victronenergy.com/v2/installations/{76095}/system-overview"
print(api_url)
response = session.get(api_url, headers=headers, verify=VERIFY_SSL)
records = response.json()
print(json.dumps(records, indent=4, sort_keys=True), file=file)
file.close()