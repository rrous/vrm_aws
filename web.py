#from ast import parse
from flask import Flask

import json
from json2html import *

from GetVrmRestData import prepareData

import boto3

from datetime import *
from dateutil import parser
from Auth import DB_NAME, PROXY_NAME, TBL_NAME, VERIFY_SSL, VRM_LOGIN, VRM_PASS, INST_ID

def AWS_GetLastRecordTime(): ### TO DEL from there
    client = boto3.client('timestream-query', verify=VERIFY_SSL)
    query_string = f"SELECT time FROM {DB_NAME}.{TBL_NAME} ORDER BY time DESC LIMIT 1"
    try:
        result = client.query(QueryString=query_string)
    except Exception as err:
        print("Exception while running query:", err)
    return parser.parse(result['Rows'][0]['Data'][0]['ScalarValue'])

app = Flask('app')

@app.route('/')
def run():
    start = AWS_GetLastRecordTime() + timedelta(hours=1)
    #start = datetime.now() - timedelta(hours=72)
    start = start.replace(minute=0, second=0, microsecond=0)
    end = datetime.now() - timedelta(hours=3)
    end = end.replace(minute=59, second=58, microsecond=0)
    print(start,'  ', end)
    readyData = prepareData(start,end)

    with open(str(date.today()) + '.json', 'w') as file:
        print(json.dumps(readyData, indent=4, sort_keys=False), file=file)

    return json2html.convert(json=readyData) + '<br><p>Totals:</p>' #+ json2html.convert(json=a['totals'])

app.run(host = '0.0.0.0', port = 8080)