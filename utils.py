"""
Helper functions to the api
"""
from datetime import datetime
from math import ceil, floor
from time import strftime

def datetime_to_epoch(time):
    """
    Converts a given datetime object to an epoch timestamp
    @param - time
 
    Returns int
    """
    diff = time - datetime.utcfromtimestamp(0)
    return int(floor(diff.total_seconds()))

def isfloat(num):
    try:
        float(num)
        return True
    except ValueError:
        return False
    except:
        return False

# #print(datetime(2018, 5, 12))
# a = datetime_to_epoch(datetime(2022,1,1))
# b = datetime.fromtimestamp(a)
# print(f"{a}: {b}")
# a = 1644937590000 / 1000
# b = datetime.fromtimestamp(a)
# print(f"{a}: {b}")
