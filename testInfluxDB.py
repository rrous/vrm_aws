#!/usr/bin/env python3

#from influxdb import InfluxDBClient
import uuid
import random
import time
import requests

INFLUX_URL='127.0.0.1'
ORG='ORG'
BUCKET_NAME='Bucket'
QUERY_URI='http://{}:9999/api/v2/write?org={}&bucket={}&precision=ms'.format(INFLUX_URL,ORG,BUCKET_NAME)
INFLUX_TOKEN='ENHlvStGlx8VHrsuywJhRGkbOyY38p4fCAATiJtU5xmhWAPyfLDUMhgqI-78sNTbyINteYpZ2e6eggGd5P9gSg=='
headers = {}
headers['Authorization'] = 'Token {}'.format(INFLUX_TOKEN)

#client = InfluxDBClient(host=INFLUXDB_HOST, port=9999)
#client.create_database('writetest')

measurement_name = 'm1'
number_of_points = 100000
batch_size = 1000
data_end_time = int(time.time() * 1000) #milliseconds

location_tags = [ "reservoir",
"orchard",
"vineyard",
"quarry",
"hospital",
"bakery",
"warehouse",
"outhouse",
"restaurant",
"cafeteria",
"delicatessen",
"office"]

fruit_tags = [ "apple",
"banana",
"cantaloupe",
"cherry",
"coconut",
"durian",
"fig",
"gooseberry",
"grape",
"grapefruit",
"guava",
"lemon",
"lime",
"lychee",
"mango",
"papaya",
"passionfruit",
"peach",
"pineapple",
"plum",
"strawberry",
"tangerine",
"tomato",
"watermelon"]

id_tags = []
for i in range(100):
    id_tags.append(str(uuid.uuid4()))

data = []
data.append("{measurement},location={location},fruit={fruit},id={id} x={x},y={y},z={z}i {timestamp}"
            .format(measurement=measurement_name,
                    location=random.choice(location_tags),
                    fruit=random.choice(fruit_tags),
                    id=random.choice(id_tags),
                    x=round(random.random(),4),
                    y=round(random.random(),4),
                    z=random.randint(0,50),
                    timestamp=data_end_time))
current_point_time = data_end_time
for i in range(number_of_points-1):
    current_point_time = current_point_time - random.randint(1,100)
    data.append("{measurement},location={location},fruit={fruit},id={id} x={x},y={y},z={z}i {timestamp}"
                .format(measurement=measurement_name,
                        location=random.choice(location_tags),
                        fruit=random.choice(fruit_tags),
                        id=random.choice(id_tags),
                        x=round(random.random(),4),
                        y=round(random.random(),4),
                        z=random.randint(0,50),
                        timestamp=current_point_time))

if __name__ == '__main__':
  # Check to see if number of points factors into batch size
  if (  number_of_points % batch_size != 0 ):
    raise SystemExit( 'Number of points must be divisible by batch size' )
  # Newline delimit the data
  
  client_write_start_time = time.perf_counter()

  for batch in range(0, len(data), batch_size):
    current_batch = '\n'.join( data[batch:batch + batch_size] )
    r = requests.post(QUERY_URI, data=current_batch, headers=headers)
    print(r.status_code)
  client_write_end_time = time.perf_counter()

  print("Client Library Write: {time}s".format(time=client_write_end_time - client_write_start_time))



# r = requests.post(QUERY_URI, data=data[0], headers=headers)
# print( r.status_code )
